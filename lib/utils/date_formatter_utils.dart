import 'package:intl/intl.dart';

String getTimeWithFormat(String formattedTime) {
  var date =
      DateFormat("yyyy-MM-dd HH:mm:ss").parseUTC(formattedTime).toLocal();

  return DateFormat("dd MMM yyyy hh:mm").format(date);
}
