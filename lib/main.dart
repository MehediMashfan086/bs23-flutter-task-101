import 'package:bs23_flutter_task_101/bloc/repositories_bloc.dart';
import 'package:bs23_flutter_task_101/repo/repositories_repo.dart';
import 'package:bs23_flutter_task_101/screens/home_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

void main() {
  runApp(RepositoryProvider(
    create: (context) => RepositoriesRepo(),
    child: const MyApp(),
  ));
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => RepositoriesBloc(RepositoriesRepo()),
      child: const MaterialApp(
        debugShowCheckedModeBanner: false,
        home: HomeScreen(),
      ),
    );
  }
}
