import 'package:bs23_flutter_task_101/bloc/repositories_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:bs23_flutter_task_101/utils/date_formatter_utils.dart';

class RepoDetailsPage extends StatelessWidget {
  final String ownerLogin;
  final String avatarUrl;
  final String? description;
  final String updatedAt;
  final String createdAt;

  const RepoDetailsPage({
    Key? key,
    required this.ownerLogin,
    required this.avatarUrl,
    this.description,
    required this.updatedAt,
    required this.createdAt,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Repository Details'),
      ),
      body: BlocBuilder<RepositoriesBloc, RepositoriesState>(
        builder: (context, state) {
          if (state is RepositoriesLoadingState) {
            return const Center(
              child: CircularProgressIndicator.adaptive(),
            );
          } else {
            return buildRepoDetails();
          }
        },
      ),
    );
  }

  Widget buildRepoDetails() {
    return Container(
      padding: const EdgeInsets.all(32),
      child: Column(
        children: [
          CircleAvatar(
            radius: 100,
            backgroundImage: NetworkImage(avatarUrl),
          ),
          const SizedBox(height: 20),
          Text(
            'UserName: $ownerLogin',
            style: const TextStyle(fontSize: 16),
          ),
          Text(
            'Description: ${description ?? 'No description'}',
            style: const TextStyle(fontSize: 16),
          ),
          Text(
            'CreatedAt: ${getTimeWithFormat(createdAt)}',
            style: const TextStyle(fontSize: 16),
          ),
          Text(
            'UpdatedAt: ${getTimeWithFormat(updatedAt)}',
            style: const TextStyle(fontSize: 16),
          ),
        ],
      ),
    );
  }
}
