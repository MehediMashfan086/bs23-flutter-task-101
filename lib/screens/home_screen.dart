import 'dart:async';

import 'package:bs23_flutter_task_101/bloc/repositories_bloc.dart';
import 'package:bs23_flutter_task_101/database/database_handler.dart';
import 'package:bs23_flutter_task_101/database/database_repo.dart';
import 'package:bs23_flutter_task_101/models/repo_models.dart';
import 'package:bs23_flutter_task_101/screens/repo_details.dart';
import 'package:bs23_flutter_task_101/utils/date_formatter_utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sqflite/sqflite.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  Database? _database;
  final ScrollController _scrollController = ScrollController();
  late List<RepositoryModel> _repositories;
  late bool _loading;
  int _currentPage = 1;
  late Timer _timer;

  bool _switchValue = false;

  @override
  void initState() {
    _repositories = [];
    _loading = false;
    // insertDB();
    context
        .read<RepositoriesBloc>()
        .add(RepositoriesLoadedEvent(page: _currentPage));
    getFromRepo();

    _scrollController.addListener(_onScroll);
    _timer = Timer.periodic(const Duration(minutes: 30), (timer) {
      _refreshData();
    });
    super.initState();
  }

  @override
  void dispose() {
    _timer.cancel();
    super.dispose();
  }

  void _onScroll() {
    if (_scrollController.position.pixels ==
            _scrollController.position.maxScrollExtent &&
        !_loading) {
      setState(() {
        _loading = true;
        _currentPage++;
      });
      context
          .read<RepositoriesBloc>()
          .add(RepositoriesLoadedEvent(page: _currentPage));
    }
  }

  void _refreshData() {
    setState(() {
      _loading = true;
      _currentPage = 1;
      _repositories.clear();
    });
    context
        .read<RepositoriesBloc>()
        .add(RepositoriesLoadedEvent(page: _currentPage));
  }

  Future<Database?> openDB() async {
    _database = await DatabaseHandler().openDB();
    return _database;
  }

  // Future<void> insertDB() async {
  //   _database = await openDB();

  //   DatabaseRepo databaseRepo = new DatabaseRepo();

  //   databaseRepo.createTable(_database);
  // }

  Future<void> getFromRepo() async {
    _database = await openDB();

    DatabaseRepo databaseRepo = new DatabaseRepo();
    await databaseRepo.getDBrepo(_database);
    await _database?.close();
  }

  void _sortDataByUpdatedAt() {
    setState(() {
      _repositories.sort((a, b) {
        if (_switchValue) {
          return b.updatedAt!.compareTo(a.updatedAt!);
        } else {
          return a.updatedAt!.compareTo(b.updatedAt!);
        }
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Center(
          child: Text(
            "Repositories List",
            style: TextStyle(color: Colors.white),
          ),
        ),
        backgroundColor: Colors.deepPurple,
        actions: [
          Switch(
            value: _switchValue,
            onChanged: (value) {
              setState(() {
                _switchValue = value;
              });
              _sortDataByUpdatedAt();
            },
          )
        ],
      ),
      body: BlocConsumer<RepositoriesBloc, RepositoriesState>(
        listener: (context, state) {
          if (state is RepositoriesLoadedState) {
            setState(() {
              _loading = false;
              _repositories.addAll(state.repositoryModel);
              if (_switchValue) {
                _sortDataByUpdatedAt();
              }
            });
          } else if (state is RepositoriesErrorState) {
            ScaffoldMessenger.of(context)
                .showSnackBar(SnackBar(content: Text(state.errorMessage)));
          }
        },
        builder: (context, state) {
          if (state is RepositoriesLoadingState && _repositories.isEmpty) {
            return const Center(
              child: CircularProgressIndicator.adaptive(),
            );
          } else {
            return ListView.builder(
              controller: _scrollController,
              itemCount: _repositories.length + (_loading ? 1 : 0),
              itemBuilder: (context, index) {
                if (index < _repositories.length) {
                  var data = _repositories[index];
                  return GestureDetector(
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => RepoDetailsPage(
                            ownerLogin: data.owner!.login.toString(),
                            avatarUrl: data.owner!.avatarUrl ?? '',
                            description: data.description ?? 'No description',
                            createdAt: data.createdAt.toString(),
                            updatedAt: data.updatedAt.toString(),
                          ),
                        ),
                      );
                    },
                    child: Card(
                      color: Colors.blueGrey,
                      child: Container(
                        padding: const EdgeInsets.all(8),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              'Repository Name: ${data.name}',
                              style: const TextStyle(fontSize: 16),
                            ),
                            Container(
                              margin: const EdgeInsets.only(top: 8),
                              child: Text(
                                'User Name: ${data.owner!.login}',
                                style: const TextStyle(fontSize: 16),
                              ),
                            ),
                            Container(
                              margin: const EdgeInsets.only(top: 8),
                              child: Text(
                                'API Endpoint: ${data.fullName}',
                                style: const TextStyle(fontSize: 16),
                              ),
                            ),
                            Container(
                              margin: const EdgeInsets.only(top: 8),
                              child: Text(
                                'UpdatedAt: ${getTimeWithFormat(data.updatedAt.toString())}',
                                style: const TextStyle(fontSize: 16),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  );
                } else {
                  return const Padding(
                    padding: EdgeInsets.all(8.0),
                    child: Center(child: CircularProgressIndicator()),
                  );
                }
              },
            );
          }
        },
      ),
    );
  }
}
