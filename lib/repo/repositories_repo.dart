import 'package:bs23_flutter_task_101/models/repo_models.dart';
import 'package:http/http.dart' as http;

class RepositoriesRepo {
  Future<List<RepositoryModel>> getRepositories(
      {required int page, required int perPage}) async {
    var response = await http.get(Uri.parse(
        "https://api.github.com/users/MehediMashfan086/repos?page=$page&per_page=$perPage"));
    if (response.statusCode == 200) {
      return repositoryModelFromJson(response.body);
    } else {
      throw Exception("Failed to load Repositories");
    }
  }
}
