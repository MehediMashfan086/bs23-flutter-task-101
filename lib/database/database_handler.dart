import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class DatabaseHandler {
  static Database? _database;
  static final DatabaseHandler _databaseHandler = DatabaseHandler._internal();

  factory DatabaseHandler() {
    return _databaseHandler;
  }

  DatabaseHandler._internal();

  Future<Database?> openDB() async {
    _database = await openDatabase(
      join(await getDatabasesPath(), 'repo.db'),
    );
    return _database;
  }
}
