part of 'repositories_bloc.dart';

sealed class RepositoriesState extends Equatable {
  const RepositoriesState();

  @override
  List<Object> get props => [];
}

final class RepositoriesInitial extends RepositoriesState {}

final class RepositoriesLoadingState extends RepositoriesState {}

final class RepositoriesLoadedState extends RepositoriesState {
  final List<RepositoryModel> repositoryModel;

  const RepositoriesLoadedState(this.repositoryModel);

  @override
  List<Object> get props => [repositoryModel];
}

final class RepositoriesErrorState extends RepositoriesState {
  final String errorMessage;

  const RepositoriesErrorState(this.errorMessage);
  @override
  List<Object> get props => [errorMessage];
}
