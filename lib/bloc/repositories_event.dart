part of 'repositories_bloc.dart';

sealed class RepositoriesEvent extends Equatable {
  const RepositoriesEvent();

  @override
  List<Object> get props => [];
}

class RepositoriesLoadedEvent extends RepositoriesEvent {
  final int page;

  const RepositoriesLoadedEvent({required this.page});

  @override
  List<Object> get props => [page];
}

class RepositoriesRefreshEvent extends RepositoriesEvent {}
