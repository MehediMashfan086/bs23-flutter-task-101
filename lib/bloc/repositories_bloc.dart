import 'package:bloc/bloc.dart';
import 'package:bs23_flutter_task_101/models/repo_models.dart';
import 'package:bs23_flutter_task_101/repo/repositories_repo.dart';
import 'package:equatable/equatable.dart';

part 'repositories_event.dart';
part 'repositories_state.dart';

class RepositoriesBloc extends Bloc<RepositoriesEvent, RepositoriesState> {
  final RepositoriesRepo repositoriesRepo;

  int currentPage = 1;
  int perPage = 10;

  RepositoriesBloc(this.repositoriesRepo) : super(RepositoriesInitial()) {
    on<RepositoriesLoadedEvent>(_onRepositoriesLoaded);
  }

  void _onRepositoriesLoaded(
      RepositoriesLoadedEvent event, Emitter<RepositoriesState> emit) async {
    try {
      emit(RepositoriesLoadingState());
      var data = await repositoriesRepo.getRepositories(
          page: currentPage, perPage: perPage);
      emit(RepositoriesLoadedState(data));
    } catch (e) {
      emit(RepositoriesErrorState(e.toString()));
    }
  }
}
